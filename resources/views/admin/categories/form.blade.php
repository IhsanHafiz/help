@extends('admin.layout')

@section('content')

@php
    $formTitle = !empty($category) ? 'Update' : 'New'
@endphp

    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-default">
                    <div class="card-header card-header-border-bottom">
                        <h2> {{ $formTitle }} Kategori</h2>
                    </div>
                    <div class="card-body">
                        @include('admin.partials.flash', ['$errors' => $errors])
                        @if (!empty($category))
                            {!! Form::model($category, ['url' => ['admin/categories', $category->id], 'method' => 'PUT']) !!}
                            {!! Form::hidden('id') !!}
                        @else
                            {!! Form::open(['url' => 'admin/categories']) !!}
                        @endif
                            <div class="form-group" id="myrequired">
                                {!! Form::label('name', 'Nama') !!}
                                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'nama kategori']) !!}
                            </div>
                            <div class="form-footer pt-5 border-top" >
                                <button type="submit" class="btn btn-primary btn-default rounded-pill btn-block"> Simpan</button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

<script src="bootstrap-validate.js"></script>
<script>
    bootstraValidate('#myrequired', 'required:Harus diisi')
</script>

@endsection